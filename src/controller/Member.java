package controller;

public class Member {
	//data members
	private String id;
	private String name;
	private String address;
	private String dateOfMembership;
	private char membershipType;
	//constructor
	public Member(String id, String name, String address, String dateOfMembership, char membershipType) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.dateOfMembership = dateOfMembership;
		this.membershipType = membershipType;
	}
	//getter setter methods
	public String getID() {
		return id;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return id + "\t" + name + "\t" + address + "\t" + dateOfMembership + "\t" + membershipType;
	}
	//end of class	
}
