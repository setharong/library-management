package controller;

public class Thesis extends BookModel {
	//data members
	private String writer;
	private String type;
	//constructor
	public Thesis(String id, String title, String publisher, String yearPublished, boolean status, String writer, String type) {
		super(id, title, publisher, yearPublished, status);
		this.writer = writer;
		this.type = type;
	}
	//toString method
	@Override
	public String toString() {
		return super.toString() + "\t" + writer + "\t" + type;
	}
	//end of class
}
