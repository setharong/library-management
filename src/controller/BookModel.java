package controller;

public class BookModel {
	//data members
	private String id;
	private String title;
	private String publisher;
	private String yearPublished;
	private boolean status;
	//static member
	//public static int count;
	//constructor
	public BookModel(String id, String title, String publisher, String yearPublished, boolean status) {
		this.id = id;
		this.title = title;
		this.publisher = publisher;
		this.yearPublished = yearPublished;
		this.status = status;
	//	count++;
	}
	//getter setter methods
	public String getID() {
		return id;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	@Override
	public String toString() {		
		return id + "\t" + title + "\t" + publisher + "\t" + yearPublished + "\t" + (status?"Available":"Borrowed");
	}
	//end of class
}
