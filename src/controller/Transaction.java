package controller;

public class Transaction {
	//data members
	private int id;
	private Member member;
	private BookModel book;
	private String dateOfIssue;
	private String dueDate;
	//constructor
	public Transaction(int id,Member member,BookModel book,String dateOfIssue,String dueDate) {
		this.id = id;
		this.member = member;
		this.book = book;
		this.dateOfIssue = dateOfIssue;
		this.dueDate = dueDate;
	}
	//getter setter methods
	public int getID() {
		return id;
	}
	public BookModel getBook() {
		return book;
	}	
	@Override
	public String toString() {
		return id + "\t" + member.getName() + "\t" + book.getTitle() + "\t" + dateOfIssue + "\t" + dueDate;
	}
	//end of class
}
