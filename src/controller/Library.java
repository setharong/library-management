package controller;

import java.util.ArrayList;
import java.util.HashMap;

public class Library {
	//data members
	private HashMap<String, BookModel> books;
	private HashMap<String, Member> members;
	private ArrayList<Transaction> transactions;
	//extra member
	private int nextTransactionID;
	//constructor
	public Library() {
		this.books = new HashMap<>();
		this.members = new HashMap<>();
		this.transactions = new ArrayList<>();
		this.nextTransactionID = 1;
	}
	//getter method for extra member
	public int getNextTID() {
		return nextTransactionID;
	}
	//methods of books
	public void addBook(BookModel b) {
		books.put(b.getID(), b);
	}
	public BookModel getBook(String bookID) {
		return books.get(bookID);
	}	
	public ArrayList<BookModel> getAllBooks(){	
		return new ArrayList<BookModel>(books.values());
	}
	//methods of members
	public void addMember(Member m) {
		members.put(m.getID(), m);
	}
	public Member getMember(String memberID) {
		return members.get(memberID);
	}
	public ArrayList<Member> getAllMembers(){
		return new ArrayList<Member>(members.values());
	}
	//methods of transactions
	public void addTransaction(Transaction t) {
		if(t.getID() == nextTransactionID) {
			t.getBook().setStatus(false);
			transactions.add(t);
			nextTransactionID ++;
		}
	}
	public void removeTransaction(Transaction t) throws NullPointerException {
		t.getBook().setStatus(true);
		transactions.remove(t);
	}
	public Transaction getTransaction(int tID) {
		for(int index = 0; index < transactions.size(); index ++) { 
			if(transactions.get(index).getID() == tID) {
				return transactions.get(index);
			}
		}
		return null;
	}
	public ArrayList<Transaction> getAllTransactions(){
		return transactions;
	}
	//end of class
}

