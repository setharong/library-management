package model;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		//about us
		ConsoleFunctions.aboutUs();
		sc.nextLine();
		//starting the menu
		do {
			ConsoleFunctions.mainMenus();	
			System.out.print("Do you want to exit? (Y/N) ");
		}while(!sc.nextLine().equalsIgnoreCase("y"));
		//end the menu
		System.out.println("Program Ended.");
		
	}

}
